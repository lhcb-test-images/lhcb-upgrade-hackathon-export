Utility container to build LHCb RPMs using fpm
==============================================

This container has the tools needed to build RPMs for the LHCb upgrade test.
It depends on the conatiner featuring all content, and features a script that produces the RPMs and adds the to the export directory.


How to build the image
----------------------

Provided that docker is installed, change to the project directory and call

```
make
```

How to create tar files for the LHCb Software
---------------------------------------------

```
mkdir rpms
docker run -v `pwd`/rpms:/export  gitlab-registry.cern.ch/lhcb-test-images/lhcb-upgrade-hackathon-rpmbuild
```

This packages the content of the /opt directory to 4 tar files:
  - LHCbCond.tar.gz
  - LHCbTest.tar.gz
  - LHCbSoft.tar.gz
  - LHCbNightlies.tar.gz 


How to create the RPMs
----------------------

```
mkdir rpms
docker run -v `pwd`/rpms:/export gitlab-registry.cern.ch/lhcb-test-images/lhcb-upgrade-hackathon-rpmbuild /build-rpm.sh
```

The directory RPMs should then (after a rather long time) contain the needed RPMs


How to release them
-------------------

```
scp rpms/LHCb\*rpm lxplus:/eos/project/l/lhcbwebsites/www/lhcb-rpm/upgrade-test
ssh lxplus7 createrepo --workers=10 /eos/project/l/lhcbwebsites/www/lhcb-rpm/upgrade-test
```


