
build:
	docker build --network=host -t gitlab-registry.cern.ch/lhcb-test-images/lhcb-upgrade-hackathon-rpmbuild .

push: build
	docker push gitlab-registry.cern.ch/lhcb-test-images/lhcb-upgrade-hackathon-rpmbuild
