FROM gitlab-registry.cern.ch/lhcb-test-images/lhcb-upgrade-hackathon

RUN yum -y install ruby-devel rpm-build

RUN gem install --no-ri --no-rdoc fpm

RUN mkdir /export

RUN echo 'cd /export' > /build-rpm.sh  && \
    echo 'fpm -s dir -t rpm --no-auto-depend -n LHCbNighties /opt/nightlies' >> /build-rpm.sh && \
    echo 'fpm -s dir -t rpm --no-auto-depend -n LHCbCond /opt/git-conddb' >> /build-rpm.sh && \
    echo 'fpm -s dir -t rpm --no-auto-depend -n LHCbTest /opt/test' >> /build-rpm.sh && \
    echo 'fpm -s dir -t rpm --no-auto-depend -n LHCbSoft /opt/lhcb' >> /build-rpm.sh && \
    chmod +x /build-rpm.sh

RUN echo 'cd /export' > /build-tar.sh  && \
    echo 'tar zcf LHCbNightlies.tar.gz /opt/nightlies' >> /build-tar.sh && \
    echo 'tar zcf LHCbCond.tar.gz /opt/git-conddb' >> /build-tar.sh && \
    echo 'tar zcf LHCbTest.tar.gz /opt/test' >> /build-tar.sh && \
    echo 'tar zcf LHCbSoft.tar.gz /opt/lhcb' >> /build-tar.sh && \
    chmod +x /build-tar.sh


CMD /build-tar.sh
